jQuery(document).ready(function ($) {
    if ($('div').is('#main_map')) {
        var locations = [];
        for (var k = 0; k < loc.length; k++) {
            locations[k] = {name: loc[k].name, lat: loc[k].lat, lng: loc[k].lng, loc: loc[k].loc};

            var center = new google.maps.LatLng(locations[k].lat, locations[k].lng);
            if (map === undefined) {
                var map = new google.maps.Map(document.getElementById('main_map'), {
                    zoom: 10,
                    center: center,
                });
            }
            var infowindow = new google.maps.InfoWindow();

            var marker;

            marker = new google.maps.Marker({
                position: center,
                map: map
            });

            google.maps.event.addListener(marker, 'click', (function (marker, k) {
                return function () {
                    infowindow.setContent('<p> Shop name: ' + locations[k].name + '</p> Shop location: ' + locations[k].loc);
                    infowindow.open(map, marker);
                }
            })(marker, k));
        }
    }
});

